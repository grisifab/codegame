#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <stdbool.h>

#define MIN(a,b) (((a)<(b))?(a):(b))
#define MAX(a,b) (((a)>(b))?(a):(b))
/**
 * Save humans, destroy zombies!
 **/
//humains ind+1 car ind0 = moi
int human_count; // +1 pour moi !
bool HViv [100];
int Hx [100];
int Hy [100];

//zombies
int zombie_count;
bool ZViv [100];
int Zx [100];
int Zy [100];
int ZNx [100];
int ZNy [100];
int ZIdH [100];
int ZKillH [100];
int ZFucked [100];

float calculDist (int IdZ, int IdH)
    {
        return (sqrt((Zx[IdZ]-Hx[IdH])*(Zx[IdZ]-Hx[IdH]) + (Zy[IdZ]-Hy[IdH])*(Zy[IdZ]-Hy[IdH])));
    }

int IdCible(int IdZ) {
    float distZH;
    float distZHMin = calculDist (IdZ, 0); // moi
    int IdDistZHMin = 0; //moi
    int nbHverif = 1;
    int i = 1;
    while ((nbHverif < (human_count + 1)) && (i<100)) {
        if ( HViv[i] == true) {
            distZH = calculDist (IdZ, i);
            nbHverif ++;
            if (distZH < distZHMin) {
                IdDistZHMin = i;
                distZHMin = distZH;
            }
        }
        i++;
    }
    return IdDistZHMin;
}


int calculNbZKillH (int IdZ, int IdH)
    {
        int Dist = calculDist(IdZ, IdH);
        return ((Dist/400)+1);
    }

float calculDistRaw (int ax, int ay)
    {
        return (sqrt((ax-Hx[0])*(ax-Hx[0]) + (ay-Hy[0])*(ay-Hy[0])));
    }

int calculNbZFucked (int IdZ)
    {
        int ax; // coordonnées x anticipées du zombie
        int ay; // coordonnées y anticipées du zombie
        int dx = ZNx [IdZ] - Zx [IdZ]; // déplacement x du Z
        int dy = ZNy [IdZ] - Zy [IdZ]; // déplacement y du Z
        int nb = 0; // atteignable sans bouger
        
        int Dist = calculDist(IdZ, 0);
        //fprintf(stderr, "totoDist : %d \n", Dist);
        if (Dist > 2000) { // pas atteignable sans bouger
               while (Dist > 2000){
                   nb++;
                   ax = dx*nb + Zx [IdZ];
                   ay = dy*nb + Zy [IdZ];
                   Dist = calculDistRaw (ax, ay);
                   Dist = Dist - 1000 * nb;
               }
        }
        //fprintf(stderr, "totoNb : %d \n", nb);
        return (nb);
    }

int main()
{

    // game loop
    while (1) {
        
        for (int i = 0 ; i < 100 ; i++)
        {
            HViv[i] = 0;
            ZViv[i] = 0;
        }
          
        // ma position
        int x, xCible;
        int y, yCible;
        scanf("%d%d", &x, &y);
        HViv [0] = true;
        Hx [0] = x;
        Hy [0] = y;
        
        // humains
        scanf("%d", &human_count);
        for (int i = 0; i < human_count; i++) {
            int human_id;
            int human_x;
            int human_y;
            scanf("%d%d%d", &human_id, &human_x, &human_y);
            HViv [(human_id + 1)] = true;
            Hx [(human_id + 1)] = human_x;
            Hy [(human_id + 1)] = human_y;
        }
        human_count++; //moi
        
        // zombies
        scanf("%d", &zombie_count);
        for (int i = 0; i < zombie_count; i++) {
            int zombie_id;
            int zombie_x;
            int zombie_y;
            int zombie_xnext;
            int zombie_ynext;
            scanf("%d%d%d%d%d", &zombie_id, &zombie_x, &zombie_y, &zombie_xnext, &zombie_ynext);
            ZViv [zombie_id] = true;
            Zx [zombie_id] = zombie_x;
            Zy [zombie_id] = zombie_y;
            ZNx [zombie_id] = zombie_xnext;
            ZNy [zombie_id]= zombie_ynext;
            ZIdH [zombie_id] = IdCible(zombie_id);
            ZKillH [zombie_id] = calculNbZKillH (zombie_id, ZIdH [zombie_id]);
            ZFucked [zombie_id] = calculNbZFucked (zombie_id);
            fprintf(stderr, "%d %d %d %d %d %d %d %d\n", zombie_id, Zx [zombie_id], Zy [zombie_id], ZNx [zombie_id], ZNy [zombie_id], ZIdH [zombie_id], ZKillH [zombie_id],  ZFucked [zombie_id]);
        }

       // choix zombie cible : celui qui va tuer en 1er une cible protégeable
       int i = 0;
       int nbZverif = 0;
       int ZKillHMin = 100;
       int indCible =-12;
       while ((nbZverif < zombie_count) && (i<100)) {
           if ( ZViv[i] == true) {
               nbZverif ++;
               // celui qui va tuer en 1er une cible protégeable différente de moi
               if ((ZKillH[i] < ZKillHMin) && (ZKillH[i]>=ZFucked[i]) && (ZIdH [i] != 0)){
                   ZKillHMin = ZKillH[i];
                   indCible = i;
               }
           }
           i++;
        }
        if (indCible == -12) {// rien de trouvé
            i = 0;
            nbZverif = 0;
            ZKillHMin = 100;
            while ((nbZverif < zombie_count) && (i<100)) {
                if ( ZViv[i] == true) {
                   nbZverif ++;
                   // celui qui va tuer en 1er une cible protégeable y-compris moi
                   if ((ZKillH[i] < ZKillHMin) && (ZKillH[i]>=ZFucked[i])){
                       ZKillHMin = ZKillH[i];
                       indCible = i;
                   }
               }
           i++;
        }
        }
        fprintf(stderr, "%d", indCible);
        // calcul rencontre cible
        xCible =  Zx [indCible] + (ZNx [indCible] - Zx [indCible]) * ZFucked [indCible];
        xCible = MAX(0,xCible);
        xCible = MIN(16000,xCible);
        yCible =  Zy [indCible] + (ZNy [indCible] - Zy [indCible]) * ZFucked [indCible];
        yCible = MAX(0,yCible);
        yCible = MIN(16000,yCible);
       
        // Write an action using printf(). DON'T FORGET THE TRAILING \n
        // To debug: fprintf(stderr, "Debug messages...\n");

        printf("%d %d\n", xCible, yCible); // Your destination coordinates
    }

    return 0;
}
