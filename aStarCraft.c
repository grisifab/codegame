#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <stdbool.h>
#include <math.h>

// [indice du robot] [X] [Y]
// +1 si pas vide
// +2 si parcouru à G
// +4 si parcouru à D
// +8 si parcouru à H
// +16 si parcouru à B
int sol [10][19][10] = {0};

// fleches commun à tous les robots
// 01234 rien,G,D,H,B
int fli [19][10] = {0};

// Strings cumulés pour cmd
char cmd[500], chainePac[100], chaine[10];

// Pour transformer 1234 en,G,D,H,B
char dirLo[5] = {' ','L','R','U','D'};
int posX,posY,best,bestdir,oldDir;

typedef struct newPos     // définit la nouvelle position en fin de parcours
{
    int posX;    // position X à la fin du parcours
    int posY;    // position Y à la fin du parcours
    int dir;     // direction à la fin du parcours
    int nbLap;   // nombre de tours pour arriver au nouveau point
    int nbChoix; // nombre de choix possibles au nouveau point
}newPos;

newPos simu;

//porto fcts
newPos parcours(int bot, int poX, int poY, int dir, bool final);
int caseOK(int bot, int poX, int poY, int dir);
int transDir(char c);

int main()
{
    for (int i = 0; i < 10; i++) { // i y 0=>9
        char line[21];
        fgets(line, 21, stdin);
        for(int j = 0; j < 19; j++){ // j x 0=>18
            if(line[j] != '#'){
                sol[0][j][i] = 1;
            }
            if(line[j] == 'L'){
                fli[j][i] = 1;
            }
            if(line[j] == 'R'){
                fli[j][i] = 2;
            }
            if(line[j] == 'U'){
                fli[j][i] = 3;
            }
            if(line[j] == 'D'){
                fli[j][i] = 4;
            }
        }
    }
    cmd[0]='\0';
    int robot_count;
    scanf("%d", &robot_count);

    // copie du sol de robot 0 pour tous les autres robots
    if (robot_count > 1){
        for(int x = 0; x < 19; x++){
            for(int y = 0; y < 10; y++){
                for (int bot = 1; bot < robot_count; bot++){
                    sol[bot][x][y] = sol[0][x][y];
                }
            }
        }
    }

    for (int i = 0; i < robot_count; i++) {
        chainePac[0]='\0';
        chaine[0]='\0';
        int x;
        int y;
        char direction[2];
        scanf("%d%d%s", &x, &y, direction);
        posX = x;
        posY = y;
        bestdir = 1;
        oldDir = transDir(direction[0]);

        while(bestdir != -1){
            //fprintf(stderr, "---%d--- X : %d Y : %d\n", i, posX, posY);
            best = 0;
            bestdir = -1;
            // pour chaque robot
            // on simule les 4 directions en comptant les points
            // si pas de flèche à l'endroit ou est le robot
            if(fli[posX][posY] == 0){
                for (int k = 1; k <= 4; k++){
                    //fprintf(stderr, "kam : %d\n", k);
                    simu = parcours(i, posX, posY, k, 0);
                    //fprintf(stderr, "res : %d\n", simu.nbLap);
                    if(simu.nbLap > best){
                        bestdir = k;
                        best = simu.nbLap;
                    }
                }
            } else { // sinon on fait le parcours avec la position imposée
                //fprintf(stderr, "direction imposée %d\n",fli[posX][posY]);
                simu = parcours(i, posX, posY, fli[posX][posY], 0);
                //fprintf(stderr, "direction imposée post\n");
                // si c'est impossible on sort
                bestdir = (simu.nbLap > 0 ? fli[posX][posY] : -1);
                //fprintf(stderr, "fleche dir : %d res : %d\n",fli[posX][posY], simu.nbLap);
            }

            if (bestdir != -1) {    // on trouve une possibilité
                // on choisi une des 4 (max pts),
                // en posant au besoin une flèche
                oldDir = bestdir;
                if(fli[posX][posY] == 0){
                    sprintf(chaine,"%d %d %c ", posX, posY, dirLo[bestdir]);
                    strcat(chainePac,chaine);
                    fli[posX][posY] = bestdir;
                }

                // on indique les cases parcourues
                simu = parcours(i, posX, posY, bestdir, 1);
                // on met le robot à la fin de parcours précédente
                posX = simu.posX;
                posY = simu.posY;
            }
        }
        strcat(cmd,chainePac);
    }

    printf("%s\n", cmd);

    return 0;
}

newPos parcours(int bot, int poX, int poY, int dir, bool final){
    int antiFoun = 0;
    bool demiTour = false;
    int Xinit, XinitOld, XinitVold;
    int Yinit, YinitOld, YinitVold;
    Xinit = XinitOld = XinitVold = poX;
    Yinit = YinitOld = YinitVold = poY;
    newPos rep;
    rep.dir = dir;      // direction initiale
    rep.nbLap = -1;     // 1er boucle du while = pos actu
    //int res = caseOK(bot, poX, poY, dir);
    int res = 1;
    //fprintf(stderr, "parcours am \n");
    while(res != 0 && !demiTour && antiFoun<12){
        // mise à jour position si possible
        rep.posX = poX;
        rep.posY = poY;
        rep.nbLap += res;
        //if(bot == 1) {fprintf(stderr, "parcours dedans\n");}
        // ecriture du parcours si mode final et non simu
        if(final){
            sol[bot][poX][poY] += pow(2,dir);
        }

        // mise à jour direction + point de départ si flèche nouvelle direction
        if ((fli[poX][poY] != 0) && (dir != fli[poX][poY])) {
            antiFoun++;
            dir = fli[poX][poY];
            rep.dir = dir;
            XinitVold = XinitOld;
            YinitVold = YinitOld;
            XinitOld = poX;
            YinitOld = poY;
            //fprintf(stderr, "Parscours fleche %d %d %d\n",poX,poY,dir);
        }

        //on arrive sur une partie déjà faite mieux vaut s'arrêter une case avant
        if ((fli[poX][poY] != 0) && !(caseOK(bot, poX, poY, fli[poX][poY]))){
            fprintf(stderr, "Stop là %d %d",rep.posX, rep.posY);
        }

        // modif position fct dir
        switch (dir)
            {
            case 1:
            poX--;
            break;
            case 2:
            poX++;
            break;
            case 3:
            poY--;
            break;
            case 4:
            poY++;
            break;
        }
        //gestion tore
        poX = (poX>=0 ? (poX%19): (19+poX));
        poY = (poY>=0 ? (poY%10): (10+poY));
        //if(bot == 1) {fprintf(stderr, "Tore %d(%d) %d(%d)\n",poX,XinitOld, poY,YinitOld);}

        // demi tour réalisé ?
        if(((poX == Xinit) && (poY == Yinit))||((poX == XinitOld) && (poY == YinitOld))||((poX == XinitVold) && (poY == YinitVold))){
            rep.posX = poX;
            rep.posY = poY;
            demiTour = true;    // si on est revenu à notre point de départ on arrête la boucle
            //fprintf(stderr, "Demi Moore\n");
        } else {
            res = caseOK(bot, poX, poY, dir);
        }
    }
    //fprintf(stderr, "parcours pwhile\n");

    // position finale trouvée, calcul nb choix après
    // attention tore non géré
    rep.nbChoix = 0;
    if(caseOK(bot, (rep.posX-1), rep.posY, 1)){rep.nbChoix++;}   // ok par la gauche
    if(caseOK(bot, (rep.posX+1), rep.posY, 2)){rep.nbChoix++;}   // ok par la droite
    if(caseOK(bot, rep.posX, (rep.posY-1), 3)){rep.nbChoix++;}   // ok par le haut
    if(caseOK(bot, rep.posX, (rep.posY+1), 4)){rep.nbChoix++;}   // ok par le bas

    return rep;
}

int caseOK(int bot, int poX, int poY, int dir){
    // pas de sol
    if (sol[bot][poX][poY] == 0){
        return 0;
    }
    // sol jamais parcourue
    if (sol[bot][poX][poY] == 1){
        return 10;
    }
    // parcouru et pas passé par là dans la même direction
    if ((sol[bot][poX][poY] > 1) && ((sol[bot][poX][poY] & (int)pow(2,dir))==0)){
        return 1;
    } else {
        return 0;
    }

}

int transDir(char c){
    int i = 0;
    while (dirLo[i] != c){
        i++;
    }
    return i;
}

