#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <stdbool.h>
#include <math.h>
#define max(a,b) (a>=b?a:b)
#define min(a,b) (a<=b?a:b)

// définition des structures------------------------------
// sommet
typedef struct Zone
{
    int numero;
    int posX;
    int posY;
    int idDom;          // id du domineur
    int idZPro[2];      // les 2 zones les plus proches
    int DistD[4][12];   //distance de chacun des drones
    int nbDAmi;         //nb drone à nous sur zone
    int deltaD;         //nb drone enemi max sur zone
}Zone;

typedef struct Drone
{
    int numero;
    int proprio;
    int posX;
    int posY;
}Drone;

// données globales
// number of zones on the map (4 to 8)
int Z;
Zone zones[8];
int IdCibleDrone[12];//Zone Cible pour chaque Drone (-1 si pas allouée)

// ID of your player (0, 1, 2, or 3)
int ID;

// number of drones in each team (3 to 11)
int D;

// number of players in the game (2 to 4 players)
int P;

// prototypes des fonctions -------------------------------
float calculDist (int xA, int yA, int xB, int yB);
float calculProxZone(int idZone); // trouve les zones les plus proches de celle indiquée et renvoie la dist totale
bool balanceTonDrone(int idZone, int nbDrone); // modifie la table IdCibleDrone pour envoyer les drones les + proches, renvoie false si plus de drones
int calculDelta(int idZone);


int main()
{
    // boucles
    int i,j;
    int nblap = 0;

    // conditions
    bool deplacement = false, oldDepl = false; // vrai si les drones n'ont pas atteint leur cible
    bool toutOk = true; // vrai si toutes les zones choisies sont occuppées
    bool toutNOk = true; // vrai si toutes les zones choisies sont perdues :-(

    // drones
    Drone drones[4][12];
    int nballoc = 0;
    int X,Y;

    // zones
    int ZG,ZD;
    int ZC[3];
    bool ZCOk[3];//fera reference aux indices de ZC, 1 si ok
    int ZCDelta[3];//fera reference aux indices de ZC, positif si delta favorable
    int ZCnbD[3];//nb drones alloués pour chaque cible
    int IdCiDroSauv[12];//Zone Cible pour chaque Drone (-1 si pas allouée) => Sauvegarde du 1er calcul
    int xMin = 4000, xMax = 0;

    scanf("%d%d%d%d", &P, &ID, &D, &Z);
    for (i = 0; i < Z; i++) {
        // corresponds to the position of the center of a zone. A zone is a circle with a radius of 100 units.
        scanf("%d%d", &X, &Y);
        zones[i].numero = i;
        zones[i].posX = X;
        zones[i].posY = Y;
    }

    // recherche des zones extrèmes G et D
    for (i = 0; i < Z; i++) {
        if(zones[i].posX < xMin){
            ZG = i;
            xMin = zones[i].posX;
        }
        if(zones[i].posX > xMax){
            ZD = i;
            xMax = zones[i].posX;
        }
    }
    // définition des indices des 3 Zones cibles ZC1..3
    // comparaison des distances entre les 3 zones de G et D, et choix de la plus faible
    if(calculProxZone(ZG) < calculProxZone(ZD)){
        ZC[0] = ZG;
    } else{
        ZC[0] = ZD;
    }
    ZC[1] = zones[ZC[0]].idZPro[0];
    ZC[2]= zones[ZC[0]].idZPro[1];
    fprintf(stderr, "Cibles %d %d %d \n", ZC[0], ZC[1], ZC[2]);
    fprintf(stderr, "X : %d Y : %d \n", zones[ZC[0]].posX, zones[ZC[0]].posY);
    fprintf(stderr, "X : %d Y : %d \n", zones[ZC[1]].posX, zones[ZC[1]].posY);
    fprintf(stderr, "X : %d Y : %d \n", zones[ZC[2]].posX, zones[ZC[2]].posY);


    // game loop
    while (1) {
        nblap++;
        for (i = 0; i < Z; i++) {
            // ID of the team controlling the zone (0, 1, 2, or 3) or -1 if it is not controlled. The zones are given in the same order as in the initialization.
            int TID;
            scanf("%d", &TID);
            zones[i].idDom = TID;
        }
        for (i = 0; i < P; i++) {
            for (j = 0; j < D; j++) {
                // The first D lines contain the coordinates of drones of a player with the ID 0, the following D lines those of the drones of player 1, and thus it continues until the last player.
                scanf("%d%d", &X, &Y);
                drones[i][j].numero = j;
                drones[i][j].posX = X;
                drones[i][j].posY = Y;
                for (int k = 0; k < 3; k++) {
                    //calcul distance drone / zone pour zones cibles
                    zones[ZC[k]].DistD[i][j] = (int)calculDist(X, Y, zones[ZC[k]].posX, zones[ZC[k]].posY);
                }
            }
        }

        if(nblap < 2) // Initialisation
        {
            fprintf(stderr, "Initialisation\n");
            // acte 1
            //on efface les cibles de chaque drone
            for (i = 0; i < D; i++) {
                IdCibleDrone[i] = -1;
            }
            // on envoie les n drones les + proches sur ZC 2 1 0
            nballoc = 0;
            while (nballoc < D){
                for(i = 0; i<3; i++)
                {
                    if(balanceTonDrone(ZC[(2-i)], 1))
                    {
                        ZCnbD[(2-i)]++; // si drone alloué on incrémente le nb de drone de la cible.
                    }
                    nballoc++;
                }
            }
            fprintf(stderr, "%d %d %d\n", ZCnbD[0],ZCnbD[1],ZCnbD[2]);
            // sauveagarde
            for (i = 0; i < 12; i++) {
                IdCiDroSauv[i] = IdCibleDrone[i];
            }

            deplacement = true;
        }

        if(deplacement || oldDepl)
        {
            oldDepl = deplacement;
            fprintf(stderr, "Deplacement\n");
            // acte 2 4 6 8
            // pas d'action tant que tous les drones n'ont pas atteint leur cible
            // vérification cible atteinte pour chaque drone
            deplacement = false;
            for(i = 0; i < D; i++) // pour tous les drones
            {
                //calcul distance entre sa cible et le drone
                if((int)calculDist(zones[IdCibleDrone[i]].posX, zones[IdCibleDrone[i]].posY, drones[ID][i].posX, drones[ID][i].posY) >= 100)
                {
                    deplacement = true; //si distance > 100 pour au minimum un drone
                }
            }
        } else
        {
            fprintf(stderr, "Maintient\n");
            //acte 3 5 7 9
            // on vérifie que nos cibles sont atteintes et le delta
            toutOk = true;
            toutNOk = true;
            for(i = 0; i<3; i++)
            {
                fprintf(stderr, "Cible domineur %d\n", zones[ZC[i]].idDom);
                ZCOk[i] = (zones[ZC[i]].idDom == ID);
                //ZCOk[i] = ((zones[ZC[i]].idDom == ID) || (zones[ZC[i]].idDom == -1));
                ZCDelta[i] = calculDelta(ZC[i]);
                fprintf(stderr, "Zone %d Delta %d\n",ZC[i], ZCDelta[i]);
                if (ZCOk[i] == false)
                {
                    toutOk = false;
                } else 
                {
                    toutNOk = false;
                }
            }
            fprintf(stderr, "Nbtr : %d touok : %d\n", nblap, toutOk);

            if(!toutOk) // au moins une cible n'est pas dominée, mais au moins une cible dominée
            {
                fprintf(stderr, "Cible perdue \n");
                int donneur, receveur, autre, deltaDonneur = 0, deltaReceveur =-5;
                for(i = 0; i<3; i++)
                {
                    if(ZCDelta[i] > deltaDonneur) // choix du delta possitif le plus grand
                    {
                        donneur = i;
                        deltaDonneur = ZCDelta[i];
                    }
                    if((ZCDelta[i] > deltaReceveur)&&(ZCOk[i] == false)) // choix de la cible perdu avec le delta le moins mauvais
                    {
                        receveur = i;
                        deltaReceveur = ZCDelta[i];
                    }
                }
                deltaReceveur = - deltaReceveur;
                fprintf(stderr, "Donneur %d(%d) Receveur %d(%d) \n", ZC[donneur], deltaDonneur, ZC[receveur], deltaReceveur);

                if(deltaDonneur > deltaReceveur) // vérification du passage de drone possible
                {
                    fprintf(stderr, "---------FUEGO !--------\n");
                    for(i = 0; i< 3; i++) // combien de drone actuellement présents sur chaque zone et modif donneur/receveur
                    {
                        ZCnbD[i] = zones[ZC[i]].nbDAmi;
                        fprintf(stderr, "Present %d \n", ZCnbD[i]);
                        if (i == donneur) // on enlève
                        {
                            ZCnbD[i] -= (deltaReceveur + 1);
                        }
                        if (i == receveur)// on rajoute
                        {
                            ZCnbD[i] += (deltaReceveur + 1);
                        }
                        if ((i != receveur) && (i != donneur))
                        {
                            autre = i; // inchangé
                        }
                    }
                    fprintf(stderr, "Z %d(%d) Z %d(%d) Z %d(%d)\n", ZC[0],ZCnbD[0], ZC[1], ZCnbD[1], ZC[2], ZCnbD[2]);
                    // effacage
                    for (i = 0; i < D; i++) {
                        IdCibleDrone[i] = -1;
                    }
                    if(ZCDelta[autre] == 0)     // risque de perdre autre
                    {
                        balanceTonDrone(ZC[autre], ZCnbD[autre]);       // on laisse les drones dessus
                        balanceTonDrone(ZC[receveur], ZCnbD[receveur]);
                        balanceTonDrone(ZC[donneur], ZCnbD[donneur]);
                        fprintf(stderr, "Zici 1--------------");
                    } else                      // pas de risque
                    {
                        balanceTonDrone(ZC[receveur], ZCnbD[receveur]); // on limite les distances
                        balanceTonDrone(ZC[autre], ZCnbD[autre]);
                        balanceTonDrone(ZC[donneur], ZCnbD[donneur]);
                        fprintf(stderr, "Zici 2--------------");
                    }
                    deplacement = true;
                }
            }
            if(toutNOk) // aucune cible dominée
            {
                fprintf(stderr, "Toutes les cibles perdues \n");
                int donneur, receveur, autre, deltaDonneur = 0, deltaReceveur =-5;
                // a mutualiser
                for(i = 0; i< 3; i++) // combien de drone actuellement présents sur chaque zone
                { // le donneur sera la zone avec le + de drones
                    ZCnbD[i] = zones[ZC[i]].nbDAmi;
                    if(ZCnbD[i] > deltaDonneur)
                    {
                        donneur = i;
                        deltaDonneur = ZCnbD[i]; 
                    }
                }
                for(i = 0; i<3; i++)
                {
                    if((ZCDelta[i] > deltaReceveur) && (donneur != i)) // choix de la cible perdu avec le delta le moins mauvais
                    {
                        receveur = i;
                        deltaReceveur = ZCDelta[i];
                    }
                }
                deltaReceveur = - deltaReceveur;
                fprintf(stderr, "Donneur %d(%d) Receveur %d(%d) \n", ZC[donneur], deltaDonneur, ZC[receveur], deltaReceveur);

                
                fprintf(stderr, "---------ACAPULCO !--------\n");
                for(i = 0; i< 3; i++) // combien de drone actuellement présents sur chaque zone et modif donneur/receveur
                {
                    fprintf(stderr, "Present %d \n", ZCnbD[i]);
                    if (i == donneur) // on enlève
                    {
                        ZCnbD[i] = 0;
                    }
                    if (i == receveur)// on rajoute
                    {
                        ZCnbD[i] += deltaDonneur;
                    }
                    if ((i != receveur) && (i != donneur))
                    {
                        autre = i; // inchangé
                    }
                }
                fprintf(stderr, "Z %d(%d) Z %d(%d) Z %d(%d)\n", ZC[0],ZCnbD[0], ZC[1], ZCnbD[1], ZC[2], ZCnbD[2]);
                // effacage
                for (i = 0; i < D; i++) {
                    IdCibleDrone[i] = -1;
                }
                
                balanceTonDrone(ZC[autre], ZCnbD[autre]);       // on laisse les drones dessus
                balanceTonDrone(ZC[receveur], ZCnbD[receveur]);
                balanceTonDrone(ZC[donneur], ZCnbD[donneur]);
                fprintf(stderr, "Zici 3--------------");
                deplacement = true;
                
            }
        }

        // envoie des consignes
        for (i = 0; i < D; i++) {
            X = zones[IdCibleDrone[i]].posX;
            Y = zones[IdCibleDrone[i]].posY;
            fprintf(stderr, "Cibles %d\n", IdCibleDrone[i]);
            printf("%d %d\n", X, Y);
        }
    }

    return 0;
}

// fonctions -------------------------------
float calculDist (int xA, int yA, int xB, int yB)
{
    return (sqrt((xA-xB)*(xA-xB) + (yA-yB)*(yA-yB)));
}

float calculProxZone(int idZone)
{
    fprintf(stderr, "Prox %d \n", idZone);
    int i,j;
    float d0, d1 = 4000, d2 = 4000;
    for (i = 0; i < Z; i++) {// on cherche les 2 zones les plus proches
        if(i != idZone){
            d0 = calculDist(zones[idZone].posX, zones[idZone].posY,zones[i].posX, zones[i].posY);
            fprintf(stderr, "distance %d %d : %f\n", idZone, i, d0);
            if(d0< d1){ //distance plus petite que le 1er plus proche
                // le 1er passe second
                fprintf(stderr, "Prox %d premier \n", i);
                d2 = d1;
                zones[idZone].idZPro[1] = zones[idZone].idZPro[0];
                // le 1er change
                d1 = d0;
                zones[idZone].idZPro[0] = i;
            } else {
                if(d0<d2){ // +grand que 1er mais plus petit que second
                fprintf(stderr, "Prox %d second \n", i);
                    d2 = d0;
                    zones[idZone].idZPro[1] = i;
                }
            }
        }
    }
    // Calcul distances totales entre les 3 zones.
    i = zones[idZone].idZPro[0];
    d0 = calculDist(zones[idZone].posX, zones[idZone].posY,zones[i].posX, zones[i].posY);
    j = zones[idZone].idZPro[1];
    d1 = calculDist(zones[idZone].posX, zones[idZone].posY,zones[j].posX, zones[j].posY);
    d2 = calculDist(zones[i].posX, zones[i].posY,zones[j].posX, zones[j].posY);
    return(d0+d1+d2);
}

bool balanceTonDrone(int idZone, int nbDrone)
{
    // recherche des nbDrone les plus proches et non utilisés pour envoie sur cible
    int choix = 0, dist, nb = 0;

    while(nb<nbDrone)
    {
        choix = 11;// n'existe pas, utile si plus rien n'est dispo, on le mettra
        dist = 8000;
        for(int i = 0; i < D; i++){
            if((zones[idZone].DistD[ID][i]<dist) && (IdCibleDrone[i] == -1))
            {
                choix = i;
                dist = zones[idZone].DistD[ID][i];
            }
        }
        IdCibleDrone[choix] = idZone;
        nb++;
        if(choix == 11) // plus de dispo
        {
            return false;
        }
    }
    return true; // tous les drones demandés on été alloués

}

int calculDelta(int idZone)
{
    // positif si plus de drones à nous
    // pour chacun des joureurs on compte de nb de drone ds un rayon de 100
    int nbDrone[4]={0};
    int maxNb = 0;
    for(int i = 0; i < P; i++)//joueurs
    {
        for(int j = 0; j < D; j++)//Drone
        {
            if(zones[idZone].DistD[i][j] <= 100)
            nbDrone[i]++;
        }
        if(i != ID) // adversaire
        maxNb = max(maxNb,nbDrone[i]);
    }
    zones[idZone].nbDAmi = nbDrone[ID];         //nb drone à nous sur zone
    zones[idZone].deltaD = nbDrone[ID]-maxNb;   //delta / adversaire le plus présent
    return(nbDrone[ID]-maxNb);
}

