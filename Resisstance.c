#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <stdbool.h>
#include <ctype.h>

// --------------------------  définition des variables  ------------------------------
bool suivant [100001] = {false};
int indexMot [100000][2] = {0};// [index][0] du caractère départ et [index][1] du caractère fin si mot présent dans chaine
char L[100001];
char *morse[26] = {".-", "-...", "-.-.", "-..", ".","..-.", "--.", "....", "..", ".---", "-.-", ".-..", "--", "-.", "---", ".--.", "--.-", ".-.", "...", "-", "..-", "...-", ".--", "-..-", "-.--", "--.."};
char reponse[90]="\0";
int Cpt = 0;

// --------------------------  définition des structures  ------------------------------
// sommet
typedef struct NodeListElement
{
    int numero;
    struct NodeListElement *next;
}NodeListElement, *NodeList;

// liste adjacence
typedef struct AdjencyListElement
{
NodeListElement *begin;
}AdjencyListElement, *AdjencyList;

// définition du graph
typedef struct GraphElement
{
bool is_oriented;
int nb_vertices; // nb sommets
AdjencyList tab_neighbours;
//unsigned short int *suivant;
}GraphElement, *Graph;

Graph g1;

// --------------------------  définition des fonctions  ------------------------------

char* letter2Morse (char c); // lettre => morse
char* word2Morse (char* w);  // mot => morse
void findMorse (char* m);    // remplira index mot (plusieurs fois?) si présent

Graph new_graph(int vertices, bool is_oriented);
bool is_empty_graph(Graph g);
NodeList add_node(int x);
void add_edge(Graph g, int src, int dest);
void print_graph(Graph g);
void erase_graph(Graph g);
void bonVoisins(Graph g);
void cherche(Graph g, int index);


int main()
{
    scanf("%s", L);
    fprintf(stderr, "L : %s \n", L);
    g1 = new_graph((int)strlen(L),true);

    int N;
    scanf("%d", &N);
    for (int i = 0; i < N; i++) {
        char W[21];
        scanf("%s", W);
        fprintf(stderr, "W : %s\n", W);
        char* reponse  = word2Morse(W);
        findMorse(reponse);
    }

    //for(int i = 0; i< g1->nb_vertices; i++){
    //    fprintf(stderr, "tableau : %d %d\n", indexMot [i][0],indexMot [i][1]);
    //}
    bonVoisins(g1);
    print_graph(g1);
    cherche(g1, 0);
    erase_graph(g1);
    fprintf(stderr, "Cpt : %d\n", Cpt);

    printf("%d\n",Cpt);

    return 0;
}

char* letter2Morse (char c){
    fprintf(stderr, "l2m : %c\n", c);
    c =tolower(c);
    fprintf(stderr, "l2m : %s\n", morse[c-'a']);
    return morse[c-'a'];
}

char* word2Morse (char* w){
    reponse[0]='\0';
    for(int i = 0; i < (strlen(w));i++){     
        strcat(reponse, letter2Morse(w[i]));
        fprintf(stderr, "w2m : %s\n", reponse);
    }
    return reponse;
}

void findMorse (char* m){
    char *position=L;
    do {
        position=strstr(position, m);
        //fprintf(stderr, "fm : %s _ %s\n", position, m);
        if (position != NULL)
        {
            fprintf(stderr, "trouvé : %s \n", m);
            add_edge(g1, position-L, (position-L+ strlen(m)-1));
            indexMot [position-L][0] = 1;                   // départ
            indexMot [(position-L+ strlen(m)-1)][1] = 1;    // arrivée
            position=position+1;
        }
    } while(position!=NULL);
}

// Fonctions -------------------------------------------
Graph new_graph(int vertices, bool is_oriented)
{
    int i;
    GraphElement *element;
    element = malloc(sizeof(*element));
    if(element == NULL)
    {
        fprintf(stderr,"Erreur\n");
        exit(EXIT_FAILURE);
    }

    element->is_oriented = is_oriented;
    element->nb_vertices = vertices;

    element->tab_neighbours = malloc(vertices * sizeof(AdjencyListElement));
    //element->suivant = malloc(vertices * sizeof(u_int16_t));
    if(element->tab_neighbours == NULL)
    {
        fprintf(stderr,"Erreur \n");
        exit(EXIT_FAILURE);
    }

    for(i=0;i<element->nb_vertices;i++)
        element->tab_neighbours[i].begin = NULL;
        //element->suivant[i] = 0;
    return element;
}

bool is_empty_graph(Graph g)
{
    if(g==NULL)
        return true;
    return false;
}

NodeList add_node(int x)
{
    NodeList n = malloc(sizeof(NodeListElement));
    if(n == NULL)
    {
        fprintf(stderr,"Erreur\n");
        exit(EXIT_FAILURE);
    }
    n->numero = x;
    n->next = NULL;

    return n;
}

void add_edge(Graph g, int src, int dest)
{
    //orienté ou non
    NodeList n = add_node(dest);
    n->next = g->tab_neighbours[src].begin;
    g->tab_neighbours[src].begin = n;

    if(!g->is_oriented)// non orienté on rajoute le lien retour
    {
        n = add_node(src);
        n->next = g->tab_neighbours[dest].begin;
        g->tab_neighbours[dest].begin = n;
    }
}

void remove_edge(Graph g, int src, int dest)
{
    bool begin = true;
    // orienté ou non
    NodeList n = g->tab_neighbours[src].begin;
    NodeList nPrec = g->tab_neighbours[src].begin;
    // on progresse jusqu'à tourver la destination
    while((n->numero != dest) && (n != NULL))
    {
        nPrec = n;
        n = n->next;
        begin = false;
    }
    // on remplace le lien du précédent vers n par celui vers n+1
    if(begin)
    {
        g->tab_neighbours[src].begin = n->next;
    } else {
        nPrec->next = n->next;
    }


    if(!g->is_oriented)// non orienté on enleve le lien retour
    {
        begin = true;
        n = g->tab_neighbours[dest].begin;
        nPrec = g->tab_neighbours[dest].begin;
        while((n->numero != src) && (n != NULL))
        {
            nPrec = n;
            n = n->next;
            begin = false;
        }
         // on remplace le lien du précédent vers n par celui vers n+1
        if(begin)
        {
            g->tab_neighbours[dest].begin = n->next;
        } else {
            nPrec->next = n->next;
        }
    }
}

void print_graph(Graph g)
{
    int i;
    fprintf(stderr, "nb pts : %d \n",g-> nb_vertices);
    for(i=0;i<g-> nb_vertices; i++)
    {
        NodeList n = g-> tab_neighbours[i].begin;
        fprintf(stderr, "(%d) : ",i);
        while(n!= NULL)
        {
            fprintf(stderr, "%d : ",n->numero);
            n = n->next;
        }
        fprintf(stderr, "NULL\n");
    }
}

void erase_graph(Graph g)
{
    if(is_empty_graph(g))
    {
        printf("rien à effacer\n");
        return;
    }
    //sommets adjacents
    if(g-> tab_neighbours)
    {
        int i;
        for(i=0;i< g-> nb_vertices;i++)
        {
            NodeList n = g-> tab_neighbours[i].begin;
            while(n!= NULL)
            {
                NodeList tmp = n;
                n=n-> next;
                free(tmp);
            }
        }
    }
    //graph
    free(g-> tab_neighbours);
    //free(g-> suivant);
}

void cherche(Graph g, int index){
    fprintf(stderr, "index dep : %d\n", index);
    NodeList n = g-> tab_neighbours[index].begin;
    while(n!= NULL)
    {
        fprintf(stderr, "index arr : %d\n", n->numero);
        if(n->numero == (g->nb_vertices-1)){ // on arrive à la fin de la chaine !
            Cpt++;
        } else {
            //if(g->suivant[n->numero])// evite de parcourir la case si cul de sac
            if(suivant[n->numero])// evite de parcourir la case si cul de sac
            cherche(g, n->numero);
        }
        n=n-> next;
    }
}

void bonVoisins(Graph g){
    for(int i = 0; i < g->nb_vertices -1; i++){
        if(indexMot [i][1] && indexMot [i+1][0]){
            add_edge(g, i, i+1);    // lier 2 mots mitoyens
            //g->suivant[i] = 1;      // indiquant que i n'est pas un cul de sac
            //g->suivant[i+1] = 1;      // indiquant que i n'est pas un cul de sac
            suivant[i] = 1;      // indiquant que i n'est pas un cul de sac
            suivant[i+1] = 1;      // indiquant que i n'est pas un cul de sac
        }
    }
    //g->suivant[(g->nb_vertices-1)]=1;   // indiquant l'arrivée
    suivant[(g->nb_vertices-1)]=1;   // indiquant l'arrivée
}
