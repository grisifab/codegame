#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <stdbool.h>
#include <math.h>
#define max(a,b) (a>=b?a:b)
#define min(a,b) (a<=b?a:b)

//définition du relief
int surface_n;
int solX [30];
int solY [30];
//position vaisseau
int X,Xcible,XcibleG,XcibleD;
int Y,Ycible;
// the horizontal speed (in m/s), can be negative.
int h_speed;
// the vertical speed (in m/s), can be negative.
int v_speed;
// the quantity of remaining fuel in liters.
int fuel;

float calculVlimH(){
    //horizontal en valeur absolue
    // entrée X,Xcible;
    double deltaX = abs(X-Xcible);
    double vlimH = 1.344*pow(deltaX,0.529);
    return ((float) vlimH);
}

void calculCible(){
    int i=1;
    int indDCible;
    while(i < surface_n){
        if ((solY[i] == solY[(i-1)]) && ((solX[i] - solX[(i-1)]) >= 1000)){
            indDCible = i;
        }
        i++;
    }
    XcibleG = solX[indDCible-1];
    XcibleD = solX[indDCible];
    Ycible = solY[indDCible];
    
    // choix du X
    if(X<solX[(indDCible-1)]){ // on est à gauche de la cible
        Xcible = solX[(indDCible-1)] + 100; // hypothèse vitesse faible
        if(h_speed > calculVlimH()) {
            Xcible = solX[indDCible] - 100; // si V forte on prend l'autre point
        }
    } else {
        if(X>solX[indDCible]){ // on est à droite de la cible
            Xcible = solX[indDCible] - 100;
            if(h_speed < -calculVlimH()) { // hypothèse vitesse faible
                Xcible = solX[(indDCible-1)] + 100; // si -V forte on prend l'autre point
            }
        } else { // on est à la verticale de la cible 
            if(h_speed > 0) {
                Xcible = solX[indDCible] - 100; // si V forte on prend l'autre point
            } else {
                 Xcible = solX[(indDCible-1)] + 100;
            }
        }
    }
    fprintf(stderr, "%d %d \n", Xcible, Ycible);
}


float calculVlim(){
    //vertical
    double h = calculDistSol();
    double vlim = 1.12*(h/1000)*(h/1000) - 0.012*h - 20.2;
    fprintf(stderr, "%f %f \n", (float)h, (float)vlim);
    return ((float) vlim);
}

int calculDistSol(){
    // a ameliorer
    return(Y - Ycible);
}

int signOf(int a, int b){
    int c = a-b;
    c = min(c,1);
    c = max(c,-1);
    return(c);
}

float calculDist (int ax, int ay, int bx, int by)
    {
        return (sqrt((ax-bx)*(ax-bx) + (ay-by)*(ay-by)));
    }
    
float calculAngle ()
    {
        float a = abs(X-Xcible);
        float b = calculDist (X, Y, Xcible, Ycible);
        float c = abs(Y-Ycible);
        float cosAlpha = (a*a + b*b - c*c)/(2 * a * b);
        fprintf(stderr, "angle %f\n", cosAlpha);
        return (acosf(cosAlpha)*180/3.14);
    }

int main()
{
    // the number of points used to draw the surface of Mars.
    int sens;
    scanf("%d", &surface_n);
    for (int i = 0; i < surface_n; i++) {
        // X coordinate of a surface point. (0 to 6999)
        int land_x;
        // Y coordinate of a surface point. By linking all the points together in a sequential fashion, you form the surface of Mars.
        int land_y;
        scanf("%d%d", &land_x, &land_y);
        solX[i]=land_x;
        solY[i]=land_y;
    }
    calculCible();

    // game loop
    while (1) {
        
        // the rotation angle in degrees (-90 to 90).
        int rotate;
        // the thrust power (0 to 4).
        int power;
        
        scanf("%d%d%d%d%d%d%d", &X, &Y, &h_speed, &v_speed, &fuel, &rotate, &power);
        
        
        if (v_speed < calculVlim()) { // ralentissement pour attérrissage
            power = 4;
            rotate = 0;
        } else {
            if(calculAngle()<25){
                power=(Y>=2800?3:4);
                if(h_speed > (signOf(Xcible,X)*calculVlimH()*0.5)){
                    sens = 1;
                } else { 
                    sens = -1;
                }
                rotate = sens * 15;    
            } else {     
                if((X>XcibleG)&&(X<XcibleD)&&(abs(h_speed)<5)){
                    // X OK
                    power = 0;
                    rotate = 0;
                } else {
                    // X NOK
                    power=(Y>=2800?3:4);
                    if(h_speed > (signOf(Xcible,X)*calculVlimH())){
                        sens = 1;
                    } else { 
                        sens = -1;
                    }
                    rotate = sens * max(20,min(abs(h_speed/2),90));
                }
            }
        }
        
        // 2 integers: rotate power. rotate is the desired rotation angle (should be 0 for level 1), power is the desired thrust power (0 to 4).
        printf("%d %d\n", rotate, power);
    }

    return 0;
}
