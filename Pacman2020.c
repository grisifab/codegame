#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <stdbool.h>
#include <math.h>
#define max(a,b) (a>=b?a:b)
#define min(a,b) (a<=b?a:b)

// définition des structures------------------------------
// sommet
typedef struct NodeListElement     // définit chaque case de la matrice
{
    unsigned short int type;    // 0 mur 1 couloir 2 intersection
    unsigned short int nbPts;   // accessible sur les 4 directions
    unsigned short int nbPsg;   // nombre de passage des pacmans de mon équipe
    unsigned short int nbPmn;   // nombre de pacmans sur les 4 directions même calcul que pour les nbPts
}NodeListElement;

NodeListElement **Carto;

int pacX[5],pacY[5],oldpacX[5],oldpacY[5],CibpacX[5],CibpacY[5],kold[5],disBoost[5],forme[5]; // position pacman amis et dispo du boost forme
bool alive[5];
int distPac[5], indPacPro[5];   // distance du pacman le plus proche et son indice
int EpacX[5],EpacY[5],EdisBoost[5],Eforme[5];  // position pacman ennemis dispo du boost forme
bool Ealive[5];
// size of the grid
int width;
// top left corner is (x=0, y=0)
int height;

//prototypes des fonctions -------------------------------

// allocation mémoire de la Carto
void malloQ(int nbX, int nbY);

// pour une pastille ou un pacman incrémentera la valeur associé (type)
// des intersections ds les 4 directions
// type 1 : pastille, 2 : pacman
void cummulIntersec(int X, int Y, int val, int typ);

// envoie des pacman vers les intersections avec le plus de points (et le moins de pacman)
void balanceTonPacman(int indice);

// passage en 0 1 2 de Pierre feuille ciseau
int pierFeuilCiso(char type_id[10]);

void calculDist(int ind);

// Main --------------------------------------------------
int main()
{
    bool init[5] = {true,true,true,true,true};
    int i,j,k;
    char cmd[100], chaine[100];

    scanf("%d%d", &width, &height); fgetc(stdin);
    fprintf(stderr, "X : %d, Y : %d\n", width, height);
    malloQ(width, height);
    for (i = 0; i < height; i++) {
        // one line of the grid: space " " is floor, pound "#" is wall
        char row[width + 2];
        fgets(row, width + 2, stdin);
        for (j = 0; j< width; j++){
            Carto[j][i].type = (row[j] == '#'? 0 : 1);
            Carto[j][i].nbPsg = 0;
        }
    }

    // définition des sommets (intersections) et angles
    for (i = 0; i < height; i++) {
        for (j = 0; j< width; j++){
            if(Carto[j][i].type == 1)   //pas un mur
            {   // calcul somme des cases mitoyennes
                k = min(1,(Carto[(j+1)%width][i].type)) + min(1,(Carto[((j-1)<0?(width-1):(j-1))][i].type))+min(1,(Carto[j][(i+1)].type))+min(1,(Carto[j][(i-1)].type));
                if (k>2) // intersection
                {
                    Carto[j][i].type = 2;
                }
                else
                {
                    if (k>=1) // angle droit ou cul de sac
                    {   // G <> D ou H <> B
                        if((min(1,(Carto[(j+1)%width][i].type)) != min(1,(Carto[((j-1)<0?(width-1):(j-1))][i].type))) || (min(1,(Carto[j][(i+1)].type)) != min(1,(Carto[j][(i-1)].type))))
                        {
                            Carto[j][i].type = 2;
                        }
                    }
                }
            }
        }
    }


    for (i = 0; i < height; i++) {
        for (j = 0; j< width; j++){
            fprintf(stderr, "%d ",Carto[j][i].type);
        }
        fprintf(stderr, "\n");
    }

    // game loop
    while (1) {

        //effacement nb pts et de pacman
        for (i = 0; i < width; i++) {
            for (j = 0; j< height; j++){
                Carto[i][j].nbPts = 0;
                Carto[i][j].nbPmn = 0;
            }
        }

        //effacement vecteur pacman en vie
        for(i = 0; i < 5; i++)
        {
            alive[i] = false;
            Ealive[i] = false;
        }

        int my_score;
        int opponent_score;
        scanf("%d%d", &my_score, &opponent_score);
        // all your pacs and enemy pacs in sight
        int visible_pac_count;
        scanf("%d", &visible_pac_count);

        for (i = 0; i < visible_pac_count; i++) {
            // pac number (unique within a team)
            int pac_id;
            // true if this pac is yours
            bool mine;
            // position in the grid
            int x;
            // position in the grid
            int y;
            // unused in wood leagues
            char type_id[10];
            // unused in wood leagues
            int speed_turns_left;
            // unused in wood leagues
            int ability_cooldown;
            int _mine;
            scanf("%d%d%d%d%s%d%d", &pac_id, &_mine, &x, &y, type_id, &speed_turns_left, &ability_cooldown);
            mine = _mine;
            if(mine) // mes Pacmans
            {
                alive[pac_id] = true;
                pacX[pac_id] = x;
                pacY[pac_id] = y;
                forme[pac_id] = pierFeuilCiso(type_id);
                disBoost[pac_id] = ((ability_cooldown == 0)?1:0);
                Carto[x][y].nbPsg++;

            }
            else
            {
                Ealive[pac_id] = true;
                EpacX[pac_id] = x;
                EpacY[pac_id] = y;
                Eforme[pac_id] = pierFeuilCiso(type_id);
                EdisBoost[pac_id] = ((ability_cooldown == 0)?1:0);
            }
            // rajout des pacmans sur la matrice aux intersections
            cummulIntersec(x, y, 1, 2);
        }

        // all pellets in sight
        int visible_pellet_count;
        scanf("%d", &visible_pellet_count);
        for (int i = 0; i < visible_pellet_count; i++) {
            int x;
            int y;
            // amount of points this pellet is worth
            int value;
            scanf("%d%d%d", &x, &y, &value);
            // rajout des pts sur la matrice aux intersections
            cummulIntersec(x, y, value, 1);
        }


        for (i = 0; i < height; i++) {
            for (j = 0; j< width; j++){
                fprintf(stderr, "%d ",Carto[j][i].nbPts);
            }
            fprintf(stderr, "\n");
        }

        cmd[0] = '\0';
        for(i = 0; i < 5; i++){     // pour tous les pacmans possibles

            if(alive[i])        // non seulement les survivants
            {
                // calcul proximité ennemi pour chaque pacman
                calculDist(i);
                fprintf(stderr, "Pac%d dist%d avec pac%d \n",i,distPac[i],indPacPro[i]);

                // calcul cible si init ou pacman arrivé à la cible ou pacman bloqué
                if((init[i]) || ((pacX[i] == CibpacX[i]) && (pacY[i] == CibpacY[i])) || ((pacX[i] == oldpacX[i]) && (pacY[i] == oldpacY[i])) )
                {
                    init[i] = false;
                    // ou qui va pacman ?
                    balanceTonPacman(i);
                }
                oldpacX[i] = pacX[i];
                oldpacY[i] = pacY[i];
                sprintf(chaine,"MOVE %d %d %d | ", i, max(min(CibpacX[i],(width-1)),0), max(min(CibpacY[i],(height-1)),0));
                fprintf(stderr, "chaine %s \n ",chaine);
                strcat(cmd,chaine);
                fprintf(stderr, "cmd %s \n ",cmd);
            }
        }

        printf("%s\n", cmd); // MOVE <pacId> <x> <y>
        //printf("MOVE %d %d %d\n", i, CibpacX[i],  CibpacY[i]); // MOVE <pacId> <x> <y>

    }
    return 0;
}

// Fonctions -------------------------------------------


void malloQ(int nbX, int nbY)
{
    Carto = malloc(nbX * 4 * sizeof(int32_t));
    for (int i = 0 ; i < nbX ; i++)
    {
        Carto[i] = malloc(nbY * 4 * sizeof(int32_t));
    }
}

void cummulIntersec(int x, int y, int value, int typ)
{
    int j;
    // rajout des pts sur la matrice
    // x/y => intersection
    if(Carto[x][y].type == 2)
    {
        if(typ == 1)Carto[x][y].nbPts += value;
        if(typ == 2)Carto[x][y].nbPmn += value;
    }
    // recherche intersections dans les 4 directions

    // parcours couloir à D
    j=1;
    while(Carto[(x+j)%width][y].type == 1)
    {
        j++;
    }
    if(Carto[(x+j)%width][y].type == 2)
    {
        if(typ == 1)Carto[(x+j)%width][y].nbPts += value;
        if(typ == 2)Carto[(x+j)%width][y].nbPmn += value;
    }

    // parcours couloir à G
    j=1;
    while(Carto[((x-j)<0?(width-j):(x-j))][y].type == 1)
    {
        j++;
    }
    if(Carto[((x-j)<0?(width-j):(x-j))][y].type == 2)
    {
        if(typ == 1)Carto[((x-j)<0?(width-j):(x-j))][y].nbPts += value;
        if(typ == 2)Carto[((x-j)<0?(width-j):(x-j))][y].nbPmn += value;
    }

    // parcours couloir en haut
    j=1;
    while(Carto[x][(y-j)].type == 1)
    {
        j++;
    }
    if(Carto[x][(y-j)].type == 2)
    {
        if(typ == 1)Carto[x][(y-j)].nbPts += value;
        if(typ == 2)Carto[x][(y-j)].nbPmn += value;
    }

    // parcours couloir en bas
    j=1;
    while(Carto[x][(y+j)].type == 1)
    {
        j++;
    }
    if(Carto[x][(y+j)].type == 2)
    {
        if(typ == 1)Carto[x][(y+j)].nbPts += value;
        if(typ == 2)Carto[x][(y+j)].nbPmn += value;
    }
}

void balanceTonPacman(int indice)
{

    int ptsCible[4][4];// [H,D,B,G][pts,x,y,pcm]
    int nbChoix = 0;
    int i,j=1,k;

    // parcours couloir en haut
    while(Carto[pacX[indice]][(pacY[indice]-j)].type == 1)
    {
        j++;
    }
    if(Carto[pacX[indice]][(pacY[indice]-j)].type == 2)
    {
        ptsCible[0][0] = Carto[pacX[indice]][(pacY[indice]-j)].nbPts;
        ptsCible[0][3] = Carto[pacX[indice]][(pacY[indice]-j)].nbPmn;
        ptsCible[0][1] = pacX[indice];
        ptsCible[0][2] = pacY[indice] - j;
        nbChoix++;
    } else // non accessible
    {
        ptsCible[0][0] = -12;
        ptsCible[0][1] = pacX[indice];
        ptsCible[0][2] = pacY[indice];
        ptsCible[0][3] = 2;
    }
    fprintf(stderr, "H : %d %d %d %d\n",ptsCible[0][0], ptsCible[0][1], ptsCible[0][2], ptsCible[0][3]);

    // parcours couloir à D
    j=1;
    while(Carto[(pacX[indice]+j)%width][pacY[indice]].type == 1)
    {
        j++;
    }
    if(Carto[(pacX[indice]+j)%width][pacY[indice]].type == 2)
    {
        ptsCible[1][0] = Carto[(pacX[indice]+j)%width][pacY[indice]].nbPts;
        ptsCible[1][3] = Carto[(pacX[indice]+j)%width][pacY[indice]].nbPmn;
        ptsCible[1][1] = (pacX[indice]+j)%width;
        ptsCible[1][2] = pacY[indice];
        nbChoix++;
    } else // non accessible
    {
        ptsCible[1][0] = -12;
        ptsCible[1][1] = pacX[indice];
        ptsCible[1][2] = pacY[indice];
        ptsCible[1][3] = 2;
    }
    fprintf(stderr, "D : %d %d %d %d\n",ptsCible[1][0], ptsCible[1][1], ptsCible[1][2], ptsCible[1][3]);

    // parcours couloir en bas
    j=1;
    while(Carto[pacX[indice]][(pacY[indice]+j)].type == 1)
    {
        j++;
    }
    if(Carto[pacX[indice]][(pacY[indice]+j)].type == 2)
    {
        ptsCible[2][0] = Carto[pacX[indice]][(pacY[indice]+j)].nbPts;
        ptsCible[2][3] = Carto[pacX[indice]][(pacY[indice]+j)].nbPmn;
        ptsCible[2][1] = pacX[indice];
        ptsCible[2][2] = pacY[indice] + j;
        nbChoix++;
    } else // non accessible
    {
        ptsCible[2][0] = -12;
        ptsCible[2][1] = pacX[indice];
        ptsCible[2][2] = pacY[indice];
        ptsCible[2][3] = 2;
    }
    fprintf(stderr, "B : %d %d %d %d\n",ptsCible[2][0], ptsCible[2][1], ptsCible[2][2], ptsCible[2][3]);

    // parcours couloir à G
    j=1;
    while(Carto[((pacX[indice]-j)<0?(width-j):(pacX[indice]-j))][pacY[indice]].type == 1)
    {
        j++;
    }
    if(Carto[((pacX[indice]-j)<0?(width-j):(pacX[indice]-j))][pacY[indice]].type == 2)
    {
        ptsCible[3][0] = Carto[((pacX[indice]-j)<0?(width-j):(pacX[indice]-j))][pacY[indice]].nbPts;
        ptsCible[3][3] = Carto[((pacX[indice]-j)<0?(width-j):(pacX[indice]-j))][pacY[indice]].nbPmn;
        ptsCible[3][1] = ((pacX[indice]-j)<0?(width-j):(pacX[indice]-j));
        ptsCible[3][2] = pacY[indice];
        nbChoix++;
    } else // non accessible
    {
        ptsCible[3][0] = -12;
        ptsCible[3][1] = pacX[indice];
        ptsCible[3][2] = pacY[indice];
        ptsCible[3][3] = 2;
    }
    fprintf(stderr, "G : %d %d %d %d\n",ptsCible[3][0], ptsCible[3][1], ptsCible[3][2], ptsCible[3][3]);

    // choix cible en fct des résultats ds les 4 directions
    bool touta0 = true;
    k = -1;
    j = -1;
    for(i = 0; i <4; i++ ){// le plus de points ou si identique on garde l'ancien choix, on évite les aller retour sauf si cul de sac
        if(((ptsCible[i][0]>j) || ((ptsCible[i][0] == j)&&(i == kold[indice])))
        && ((nbChoix == 1) || (i+kold[indice]%2 != 0) || (i == kold[indice])) && (ptsCible[i][3] < 2))
        {
            j = ptsCible[i][0];
            k = i;
        }
        if(ptsCible[i][0]>0) touta0 = false;
    }

    // on tourne en rond=> on va là ou on est passé le moins
    if(touta0)
    {
        k = -1;
        j = 1000;
        for(i = 0; i <4; i++ ){     // là ou on est passé le moins
            if((ptsCible[i][0]>=0) && (Carto[ptsCible[i][1]][ptsCible[i][2]].nbPsg<j) && (ptsCible[i][3] < 2))
            {
                j = Carto[ptsCible[i][1]][ptsCible[i][2]].nbPsg;
                k = i;
            }
        }
    }
    kold[indice] = k;
    CibpacX[indice] = ptsCible[k][1];
    CibpacY[indice] = ptsCible[k][2];
}

int pierFeuilCiso(char type_id[10])
{
    if(!strcmp(type_id,"ROCK")) return 0;
    if(!strcmp(type_id,"PAPER")) return 1;
    return 2; // SCISSORS
}

void calculDist(int ind)
{
    int indPP,dx,dy,dist = 99;
    for(int i= 0; i < 5; i++)
    {
        if (Ealive[i])
        {
            dx = abs(pacX[ind] - EpacX[i]);
            dx = min(dx, (width-dx)); // à la grosse...
            dy = abs(pacY[ind] - EpacY[i]);
            if(dist <(dx + dy))
            {
                dist = dx + dy;
                indPP = i;
            }
        }
    }
    distPac[ind] = dist;
    indPacPro[ind] = indPP;
}




