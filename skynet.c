#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <stdbool.h>
#include <math.h>
#define max(a,b) (a>=b?a:b)
#define min(a,b) (a<=b?a:b)

// définition des structures------------------------------
// sommet
typedef struct NodeListElement
{
    int numero;
    struct NodeListElement *next;
}NodeListElement, *NodeList;

// liste adjacence
typedef struct AdjencyListElement
{
NodeListElement *begin;
}AdjencyListElement, *AdjencyList;

// définition du graph
typedef struct GraphElement
{
bool is_oriented;
int nb_vertices; // nb sommets
AdjencyList tab_neighbours;
unsigned short int *HauteurList;
}GraphElement, *Graph;

//prototypes des fonctions -------------------------------
Graph new_graph(int vertices, bool is_oriented);
bool is_empty_graph(Graph g);
NodeList add_node(int x);
void add_edge(Graph g, int src, int dest);
void defineExit(Graph g, int ext);
void defineHauteur(Graph g, int rang);
void remove_edge(Graph g, int src, int dest);
void print_graph(Graph g);
void erase_graph(Graph g);
int choixLien(Graph g, int index);


// Main --------------------------------------------------
int main()
{
    // the total number of nodes in the level, including the gateways
    int N,Nam,Nav;
    // the number of links
    int L;
    // the number of exit gateways
    int E;
    scanf("%d%d%d", &N, &L, &E);
    // initialisation du graph avec le nb de sommet
    Graph g1 = new_graph(N,false);
    
    for (int i = 0; i < L; i++) {
        // N1 and N2 defines a link between these nodes
        int N1;
        int N2;
        scanf("%d%d", &N1, &N2);
        // rajout des arrêtes
        add_edge(g1, N1, N2);
    }
    
    for (int i = 0; i < E; i++) {
        // the index of a gateway node
        int EI;
        scanf("%d", &EI);
        // définition des sorties
        defineExit(g1, EI);
    }
    
    // calcul de la proximité des pts avec les sorties + haut = +proche
    defineHauteur(g1, 2);
    // affichage connextion et hauteur
    print_graph(g1);

    // game loop
    while (1) {
       // The index of the node on which the Skynet agent is positioned this turn
        int SI;
        scanf("%d", &SI);
        // calcul du lien à casser
        Nav = choixLien(g1, SI);
        
        // Example: 0 1 are the indices of the nodes you wish to sever the link between
        printf("%d %d\n",SI,Nav);

        // on enleve le lien cassé
        remove_edge(g1,SI,Nav);
        // on refait les calculs
        defineHauteur(g1, 8);
        print_graph(g1);
    }
    erase_graph(g1);
    return 0;
}


// Fonctions -------------------------------------------
Graph new_graph(int vertices, bool is_oriented)
{
    int i;
    GraphElement *element;
    element = malloc(sizeof(*element));
    if(element == NULL)
    {
        fprintf(stderr,"Erreur\n");
        exit(EXIT_FAILURE);
    }

    element->is_oriented = is_oriented;
    element->nb_vertices = vertices;

    element->tab_neighbours = malloc(vertices * sizeof(AdjencyListElement));
    element->HauteurList = malloc(vertices * sizeof(int16_t));
    if(element->tab_neighbours == NULL)
    {
        fprintf(stderr,"Erreur \n");
        exit(EXIT_FAILURE);
    }

    for(i=0;i<element->nb_vertices;i++)
        element->tab_neighbours[i].begin = NULL;
        element->HauteurList[i] = 0;
    return element;
}

bool is_empty_graph(Graph g)
{
    if(g==NULL)
        return true;
    return false;
}

NodeList add_node(int x)
{
    NodeList n = malloc(sizeof(NodeListElement));
    if(n == NULL)
    {
        fprintf(stderr,"Erreur\n");
        exit(EXIT_FAILURE);
    }
    n->numero = x;
    n->next = NULL;

    return n;
}

void add_edge(Graph g, int src, int dest)
{
    //orienté ou non
    NodeList n = add_node(dest);
    n->next = g->tab_neighbours[src].begin;
    g->tab_neighbours[src].begin = n;

    if(!g->is_oriented)// non orienté on rajoute le lien retour
    {
        n = add_node(src);
        n->next = g->tab_neighbours[dest].begin;
        g->tab_neighbours[dest].begin = n;
    }
}

void defineExit(Graph g, int ext)
{
    // on associe la valeur la plus haute à chaque sortie
    g->HauteurList[ext]=256;
}

void defineHauteur(Graph g, int rangLim)
{
    // rang par rang avec un rang max de 8 (2^0)
    // on va définir la hauteur en partant du plus haut (sortie)
    // en divisant par 2 à chaque pas.
    int rang = 1;
    int i;

    // mise à zéro des hauteurs des sommets non sortie
    for(i = 0; i < g->nb_vertices; i++)
    {
        if(g->HauteurList[i] != 256)
        g->HauteurList[i] = 0;
    }

    // progression rang par rang
    while (rang <= rangLim)
    {
        for(i = 0; i < g->nb_vertices; i++)
        {
            if(g->HauteurList[i] == (pow(2,(9-rang))))
            {
                NodeList n = g-> tab_neighbours[i].begin;
                while(n!= NULL)
                {
                    g->HauteurList[n->numero] = max(g->HauteurList[n->numero], (pow(2,(8-rang))));
                    n=n-> next;
                }
            }
        }
        rang++;
    }
}

int choixLien(Graph g, int index)
{
    int hMax = 0;
    // on se place à la même pos que le virus
    // on cherche la connexion la plus proche de la sortie
    // on la cassera
    NodeList n = g-> tab_neighbours[index].begin;
    int Nav = n->numero;
    while(n!= NULL)
    {
        if(g->HauteurList[n->numero] > hMax){
            hMax = g->HauteurList[n->numero];
            Nav = n->numero;
        }
        n=n-> next;
    }
    return Nav;
}

void remove_edge(Graph g, int src, int dest)
{
    bool begin = true;
    // orienté ou non
    NodeList n = g->tab_neighbours[src].begin;
    NodeList nPrec = g->tab_neighbours[src].begin;
    // on progresse jusqu'à tourver la destination
    while((n->numero != dest) && (n != NULL))
    {
        nPrec = n;
        n = n->next;
        begin = false;
    }
    // on remplace le lien du précédent vers n par celui vers n+1
    if(begin)
    {
        g->tab_neighbours[src].begin = n->next;
    } else {
        nPrec->next = n->next;
    }


    if(!g->is_oriented)// non orienté on enleve le lien retour
    {
        begin = true;
        n = g->tab_neighbours[dest].begin;
        nPrec = g->tab_neighbours[dest].begin;
        while((n->numero != src) && (n != NULL))
        {
            nPrec = n;
            n = n->next;
            begin = false;
        }
         // on remplace le lien du précédent vers n par celui vers n+1
        if(begin)
        {
            g->tab_neighbours[dest].begin = n->next;
        } else {
            nPrec->next = n->next;
        }
    }
}

void print_graph(Graph g)
{
    int i;
    fprintf(stderr, "nb pts : %d \n",g-> nb_vertices);
    for(i=0;i<g-> nb_vertices; i++)
    {
        NodeList n = g-> tab_neighbours[i].begin;
        fprintf(stderr, "(%d) : ",i);
        while(n!= NULL)
        {
            fprintf(stderr, "%d(%d) : ",n->numero,g->HauteurList[n->numero]);
            n = n->next;
        }
        fprintf(stderr, "NULL\n");
    }
}

void erase_graph(Graph g)
{
    if(is_empty_graph(g))
    {
        printf("rien à effacer\n");
        return;
    }
    //sommets adjacents
    if(g-> tab_neighbours)
    {
        int i;
        for(i=0;i< g-> nb_vertices;i++)
        {
            NodeList n = g-> tab_neighbours[i].begin;
            while(n!= NULL)
            {
                NodeList tmp = n;
                n=n-> next;
                free(tmp);
            }
        }
    }
    //graph
    free(g-> tab_neighbours);
    free(g-> HauteurList);
}

