#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <stdbool.h>

/**
 * Auto-generated code below aims at helping you parse
 * the standard input according to the problem statement.
 **/

int main()
{
    // width of the building.
    int W;
    // height of the building.
    int H;
    scanf("%d%d", &W, &H);
    // maximum number of turns before game over.
    int N;
    scanf("%d", &N);
    int X0, XMin=0, XMax=(W-1);
    int Y0, YMin=0, YMax=(H-1);
    scanf("%d%d", &X0, &Y0);

    // game loop
    while (1) {
        // the direction of the bombs from batman's current location (U, UR, R, DR, D, DL, L or UL)
        char bomb_dir[4];
        scanf("%s", bomb_dir);
        
        // gestion Y
        if(bomb_dir[0] != 'D'){
            YMax = Y0;
        }
        if(bomb_dir[0] != 'U'){
            YMin = Y0;
        }
        if((Y0 == ((YMin+YMax)/2))&&(YMin!=YMax)){
            Y0++;
        } else {
            Y0 = (YMin+YMax)/2;
        }
        
        // gestion X
        if((bomb_dir[0] == 'L') || (bomb_dir[1] == 'L')){
            XMax = X0;
        }
        if((bomb_dir[0] == 'R') || (bomb_dir[1] == 'R')){
            XMin = X0;
        }
        if((bomb_dir[0] != 'L') && (bomb_dir[1] != 'L')&&(bomb_dir[0] != 'R') && (bomb_dir[1] != 'R')){
            XMax=XMin=X0;
        }
        if((X0 == ((XMin+XMax)/2))&&(XMin!=XMax)){
            X0++;
        } else {
            X0 = (XMin+XMax)/2;
        }

        // the location of the next window Batman should jump to.
        printf("%d %d\n", X0, Y0);
    }

    return 0;
}
