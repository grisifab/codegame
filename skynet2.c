#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <stdbool.h>
#include <math.h>
#define max(a,b) (a>=b?a:b)
#define min(a,b) (a<=b?a:b)

// définition des structures------------------------------
// sommet
typedef struct NodeListElement
{
    int numero;
    struct NodeListElement *next;
}NodeListElement, *NodeList;

// liste adjacence
typedef struct AdjencyListElement
{
    NodeListElement *begin;
}AdjencyListElement, *AdjencyList;

// définition du graph
typedef struct GraphElement
{
    bool is_oriented;
    int nb_vertices; // nb sommets
    AdjencyList tab_neighbours;
    unsigned short int *HauteurList;
    unsigned short int *ProximaList;
}GraphElement, *Graph;

//prototypes des fonctions -------------------------------
Graph new_graph(int vertices, bool is_oriented);
bool is_empty_graph(Graph g);
NodeList add_node(int x);
void add_edge(Graph g, int src, int dest);
void defineExit(Graph g, int ext);
void defineHauteur(Graph g, int rang);
void remove_edge(Graph g, int src, int dest);
void print_graph(Graph g);
void erase_graph(Graph g);
int choixLien(Graph g, int index, int indexPrec);
int distance(Graph g, int src, int dest);

// Main --------------------------------------------------
int main()
{
    // the total number of nodes in the level, including the gateways
    int N,Nprec,Nam,Nav;
    // the number of links
    int L;
    // the number of exit gateways
    int E;
    scanf("%d%d%d", &N, &L, &E);
    // initialisation du graph avec le nb de sommet
    Graph g1 = new_graph(N,false);
    for (int i = 0; i < L; i++) {
        // N1 and N2 defines a link between these nodes
        int N1;
        int N2;
        scanf("%d%d", &N1, &N2);
        // rajout des arrêtes
        add_edge(g1, N1, N2);
    }
    for (int i = 0; i < E; i++) {
        // the index of a gateway node
        int EI;
        scanf("%d", &EI);
        // définition des sorties
        defineExit(g1, EI);
    }
    // calcul de la proximité des pts avec les sorties + haut = +proche
    defineHauteur(g1, 8);
    // affichage connextion et hauteur
    print_graph(g1);

    // game loop
    while (1) {
        // The index of the node on which the Skynet agent is positioned this turn
        int SI;
        int proxMin = 50;
        int val;
        int indexDble = -12;
        scanf("%d", &SI);

        // Choix du lien à casser par ordre de priorité
        // 1) Danger direct = sur case mitoyenne à sortie, 
        // => on casse le lien vers cette sortie
        // 2) Certaines cases donnent sur deux sorties, 
        // => on casse un lien(vers sortie) de la case la plus proche
        // 3) Sinon 
        // => on casse le lien le plus proche permettant d'atteindre une sortie

        // calcul du danger immédiat
        Nprec = SI;
        Nam = SI;
        Nav = choixLien(g1, Nam, Nprec);
        fprintf(stderr, "Danger immédiat ? Nam : %d Nav : %d (%d) \n",Nam,Nav,g1->HauteurList[Nav]);

        if(g1->HauteurList[Nav] != 256) // pas de danger immédiat
        {
            // vérif si double sortie
            for(int i = 0; i < g1->nb_vertices; i++)
            {
                // donne vers 2 sorties ou plus et plus proche que le précédent
                //if(g1->HauteurList[i] > 128)
                if((g1->HauteurList[i] > 128) && (g1->HauteurList[i] < 256))
                {
                    val = distance(g1, SI, i);
                    fprintf(stderr, "Proxima (%d) : %d\n", i, val);
                    if(val < proxMin)
                    {
                        proxMin = val;
                        indexDble = i;
                    }
                }
            }
            if(indexDble != -12) // il existe une double sortie
            {
                Nam = indexDble;
                Nav = choixLien(g1, indexDble, indexDble);
                fprintf(stderr, "Choix Proxima %d : %d\n", Nam, Nav);
            } else // pas de dble sortie ni de danger immédiat => on avance rang par rang pour trouver le lien le plus proche à casser
            {
                while(g1->HauteurList[Nav] != 256)
                {
                    Nprec = Nam;
                    Nam = Nav;
                    Nav = choixLien(g1, Nam, Nprec);
                    fprintf(stderr, "Rang par rang Nam : %d Nav : %d \n",Nam,Nav);
                }
            }
        }
        
        // Example: 0 1 are the indices of the nodes you wish to sever the link between
        printf("%d %d\n",Nam,Nav);

        // on enleve le lien cassé
        remove_edge(g1,Nam,Nav);
        // on refait les calculs
        defineHauteur(g1, 8);
        print_graph(g1);
    }
    erase_graph(g1);
    return 0;
}

// Fonctions -------------------------------------------
Graph new_graph(int vertices, bool is_oriented)
{
    int i;
    GraphElement *element;
    element = malloc(sizeof(*element));
    if(element == NULL)
    {
        fprintf(stderr,"Erreur\n");
        exit(EXIT_FAILURE);
    }

    element->is_oriented = is_oriented;
    element->nb_vertices = vertices;

    element->tab_neighbours = malloc(vertices * sizeof(AdjencyListElement));
    element->HauteurList = malloc(vertices * sizeof(int16_t));
    element->ProximaList = malloc(vertices * sizeof(int16_t));
    if(element->tab_neighbours == NULL)
    {
        fprintf(stderr,"Erreur \n");
        exit(EXIT_FAILURE);
    }

    for(i=0;i<element->nb_vertices;i++)
    {
        element->tab_neighbours[i].begin = NULL;
        element->HauteurList[i] = 0;
    }
return element;
}

bool is_empty_graph(Graph g)
{
    if(g==NULL)
        return true;
    return false;
}

NodeList add_node(int x)
{
    NodeList n = malloc(sizeof(NodeListElement));
    if(n == NULL)
    {
        fprintf(stderr,"Erreur\n");
        exit(EXIT_FAILURE);
    }
    n->numero = x;
    n->next = NULL;

    return n;
}

void add_edge(Graph g, int src, int dest)
{
    //orienté ou non
    NodeList n = add_node(dest);
    n->next = g->tab_neighbours[src].begin;
    g->tab_neighbours[src].begin = n;

    if(!g->is_oriented)// non orienté on rajoute le lien retour
    {
        n = add_node(src);
        n->next = g->tab_neighbours[dest].begin;
        g->tab_neighbours[dest].begin = n;
    }
}

void defineExit(Graph g, int ext)
{
    // on associe la valeur la plus haute à chaque sortie
    g->HauteurList[ext]=256;
}

void defineHauteur(Graph g, int rangLim)
{
    // rang par rang avec un rang max de 8 (2^0)
    // on va définir la hauteur en partant du plus haut (sortie)
    // en divisant par 2 à chaque pas.
    int rang = 1;
    int i,val;

    // mise à zéro des hauteurs des sommets non sortie
    for(i = 0; i < g->nb_vertices; i++)
    {
        if(g->HauteurList[i] != 256)
        g->HauteurList[i] = 0;
    }

    // progression rang par rang
    while (rang <= rangLim)
    {
        for(i = 0; i < g->nb_vertices; i++)
        {
            if(g->HauteurList[i] == (pow(2,(9-rang))))
            {
                NodeList n = g-> tab_neighbours[i].begin;
                while(n!= NULL)
                {
                    val = g->HauteurList[n->numero];
                    // Si la hauteur est déjà égale à la valeur divisée par 2, 
                    if((val >= pow(2,(8-rang))) && (val < pow(2,(9-rang))))
                    {
                        // on incrémente pour indiquer sa priorité
                        g->HauteurList[n->numero]++;
                    } else {
                        // sinon on met la valeur divisée par 2
                        g->HauteurList[n->numero] = max(val, (pow(2,(8-rang))));
                    }
                    n=n-> next;
                }
            }
        }
        rang++;
    }
}

int choixLien(Graph g, int index, int indexPrec)
{
    int hMax = 0;
    // on se place à la même pos que le virus
    // on cherche la connexion la plus proche de la sortie
    // on la cassera
    NodeList n = g-> tab_neighbours[index].begin;
    int Nav = n->numero;
    while(n!= NULL)
    {
        if((g->HauteurList[n->numero] >= hMax)&&(n->numero != indexPrec)){
            hMax = g->HauteurList[n->numero];
            Nav = n->numero;
        }
        n=n-> next;
    }
    return Nav;
}

void remove_edge(Graph g, int src, int dest)
{
    bool begin = true;
    // orienté ou non
    NodeList n = g->tab_neighbours[src].begin;
    NodeList nPrec = g->tab_neighbours[src].begin;
    // on progresse jusqu'à tourver la destination
    while((n->numero != dest) && (n != NULL))
    {
        nPrec = n;
        n = n->next;
        begin = false;
    }
    // on remplace le lien du précédent vers n par celui vers n+1
    if(begin)
    {
        g->tab_neighbours[src].begin = n->next;
    } else {
        nPrec->next = n->next;
    }

    if(!g->is_oriented)// non orienté on enleve le lien retour
    {
        begin = true;
        n = g->tab_neighbours[dest].begin;
        nPrec = g->tab_neighbours[dest].begin;
        while((n->numero != src) && (n != NULL))
        {
            nPrec = n;
            n = n->next;
            begin = false;
        }
        // on remplace le lien du précédent vers n par celui vers n+1
        if(begin)
        {
            g->tab_neighbours[dest].begin = n->next;
        } else {
            nPrec->next = n->next;
        }
    }
}

void print_graph(Graph g)
{
    int i;
    fprintf(stderr, "nb pts : %d \n",g-> nb_vertices);
    for(i=0;i<g-> nb_vertices; i++)
    {
        NodeList n = g-> tab_neighbours[i].begin;
        fprintf(stderr, "(%d) : ",i);
        while(n!= NULL)
        {
            fprintf(stderr, "%d(%d) : ",n->numero,g->HauteurList[n->numero]);
            n = n->next;
        }
        fprintf(stderr, "NULL\n");
    }
}

void erase_graph(Graph g)
{
    if(is_empty_graph(g))
    {
        printf("rien à effacer\n");
        return;
    }
    //sommets adjacents
    if(g-> tab_neighbours)
    {
        int i;
        for(i=0;i< g-> nb_vertices;i++)
        {
            NodeList n = g-> tab_neighbours[i].begin;
            while(n!= NULL)
            {
                NodeList tmp = n;
                n=n-> next;
                free(tmp);
            }
        }
    }
    //graph
    free(g-> tab_neighbours);
    free(g-> HauteurList);
}

int distance(Graph g, int src, int dest)
{
    // tous les éléments de la table de proxima à 50 au début
    // on part de destination avec une valeur de 0
    // on donne une valeur de 1 à tous les voisins
    // on donne une valeur de 2 aux voisins de chaque voisin
    // ainsi de suite en faisant un min à chaque affectation
    int rang = 0;
    int i;
    NodeList n;

    // mise au max de tous les points
    for(i = 0; i < g->nb_vertices; i++)
    {
        g->ProximaList[i] = 50;
    }
    // sauf la destination
    g->ProximaList[dest] = 0;

    // progression rang par rang
    while ((g->ProximaList[src] == 50)&&(rang<g->nb_vertices))
    {
        for(i = 0; i < g->nb_vertices; i++)
        {
            if(g->ProximaList[i] == rang)
            {
                n = g-> tab_neighbours[i].begin;
                while(n!= NULL)
                {
                    if(g->HauteurList[n->numero] != 256) // sans traiter les sorties
                    {
                        g->ProximaList[n->numero] = min((rang+1),g->ProximaList[n->numero]);
                    }
                    n=n-> next;
                }
            }
        }
        rang++;
    }
    // comptons uniquement les rgs sans danger direct
    rang = (g->ProximaList[src]-1);
    int hMax = 0;
    int DistssDanger = 0;
    int index,indexSuivant;
    index =src;
    fprintf(stderr, "chemin : _%d_", index);
    
    while(rang>0)
    {
        hMax = 0;
        // on vérifie tous les voisins
        n = g-> tab_neighbours[index].begin;
        while(n!= NULL)
        {
            // si un voisin a le rang n-1, qu'il a l'hauteur la + élévée (sans être 256) on le sélectionne comme index suivant
            if((g->ProximaList[n->numero] == rang) && (g->HauteurList[n->numero] >= hMax) && (g->HauteurList[n->numero] < 256))
            {
                hMax = g->HauteurList[n->numero];
                indexSuivant = n->numero;
            }
            n=n-> next;
        }
        if(hMax < 128) //sans danger immédiat
        {
            DistssDanger++;
        }
        fprintf(stderr, "_%d_", indexSuivant);
        rang--;
        index = indexSuivant;
    }
    fprintf(stderr, "_%d_(%d)\n", dest, DistssDanger);

    //return(g->ProximaList[src]); distance totale
    return(DistssDanger);
}
