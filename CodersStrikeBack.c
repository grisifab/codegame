#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <stdbool.h>
#include <math.h>

/**
 * This code automatically collects game data in an infinite loop.
 * It uses the standard input to place data into the game variables such as x and y.
 * YOU DO NOT NEED TO MODIFY THE INITIALIZATION OF THE GAME VARIABLES.
 **/
 
float calculDist (int ax, int ay, int bx, int by)
    {
        return (sqrt((ax-bx)*(ax-bx) + (ay-by)*(ay-by)));
    }
    
float calculAngle (float a, float b, float c)
    {
        float cosAlpha = (a*a + b*b - c*c)/(2 * a * b);
        return (acosf(cosAlpha)*180/3.14);
    }
int main()
    {
        // nb appel fct
        int round = 0;
        // x position of your pod
        int x,lastx;
        // y position of your pod
        int y,lasty;
        // x position of the next check point
        int next_checkpoint_x;
        int last_checkpoint_x = -12;
        // y position of the next check point
        int next_checkpoint_y;
        int last_checkpoint_y = -12;
        // distance prochaine cible
        int nextCheckpointDist;
        //angle prochaine cible
        int nextCheckpointAngle;
        // puissance
        int power;
        // position x adversaire
        int opponentX;
        // position y adversaire
        int opponentY;
        // vitesse unit/loop
        float speed;
        // seuil vitesse freinage
        float vlim;
        // boost dispo
        bool dispoBoost = true;
        // pour les calculs de trajectoire
        bool firstLap = true;
        // choix cible
        bool modif = false;
        //bool modifactif = true;
        bool modifactif = false;
        bool lastCP = false;
        int nbLap = 1;
        int nbPts = 0;
        int tabXCP [10];
        int tabYCP [10];
        float distNext [10];
        float distOpp [10];
        float angle [10];
        float newX [10];
        float newY [10];
        bool newAcv [10];
        float newX1, newX2, newY1, newY2, newD1, newD2;
        int large = 1000;
        int indiceCible = 0;
    // game loop
    while (1) {
        
        
        scanf("%d%d%d%d%d%d", &x, &y, &next_checkpoint_x, &next_checkpoint_y, &nextCheckpointDist, &nextCheckpointAngle);
        scanf("%d%d", &opponentX, &opponentY);
        
        //  // enregistrement coord des CP 0 et 1
        if(round == 0){
            //fprintf(stderr, "1er round CP 0 et 1 \n");
            tabXCP[nbPts] = x;
            tabYCP[nbPts] = y;
            nbPts++;
            tabXCP[nbPts] = next_checkpoint_x;
            tabYCP[nbPts] = next_checkpoint_y;
            last_checkpoint_x = next_checkpoint_x;
            last_checkpoint_y = next_checkpoint_y;
            nbPts++;
        }
        
        // enregistrement coord des autres CP
        if((next_checkpoint_x != last_checkpoint_x) && (next_checkpoint_y != last_checkpoint_y) && firstLap ){
            last_checkpoint_x = next_checkpoint_x;
            last_checkpoint_y = next_checkpoint_y;
            if(calculDist(next_checkpoint_x,next_checkpoint_y,tabXCP[0],tabYCP[0]) < 700){
                firstLap = false;
                //fprintf(stderr, "fin du premier tour => calcul \n");
                // plus précis que position départ
                tabXCP[0] = next_checkpoint_x;
                tabYCP[0] = next_checkpoint_y;
                // calcul 
                for (int i = 0; i < nbPts; i++) {
                    int indNm1, indNp1;
                    // n - 1
                    indNm1 = i-1; 
                    if (indNm1 == -1) {indNm1 = nbPts -1;}
                    // n + 1
                    indNp1 = (i+1) % nbPts;
                    // calcul distance entre CP
                    distNext [i] = calculDist(tabXCP[i],tabYCP[i],tabXCP[indNp1],tabYCP[indNp1]);
                    distOpp [i] = calculDist(tabXCP[indNm1],tabYCP[indNm1],tabXCP[indNp1],tabYCP[indNp1]);
                }    
                for (int i = 0; i < nbPts; i++) {    
                    // calcul angle virage
                    int indNm1, indNp1;
                    // n - 1
                    indNm1 = i-1;
                    // n + 1
                    indNp1 = (i+1) % nbPts;
                    if (indNm1 == -1) {indNm1 = nbPts -1;}
                    angle[i] = calculAngle(distNext [i], distNext [indNm1], distOpp [i]);
                    if (angle[i] < 120){ // pas de trajectoire pour les angles ouverts
                        newAcv [i] = true;
                    } else {
                        newAcv [i] = false;
                    }
                    // calcul nouveau pt visé ! 
                    // attention 2 possibles on prend le plus éloigné du CP suivant
                    newX1 = tabXCP[i]-(large*(tabYCP[i] - tabYCP[indNm1])/distNext[indNm1]);
                    newY1 = tabYCP[i]-(large*(tabXCP[indNm1]-tabXCP[i])/distNext[indNm1]);
                    newD1 = calculDist(newX1,newY1,tabXCP[indNp1],tabYCP[indNp1]);
                    newX2 = tabXCP[i]+(large*(tabYCP[i] - tabYCP[indNm1])/distNext[indNm1]);
                    newY2 = tabYCP[i]+(large*(tabXCP[indNm1]-tabXCP[i])/distNext[indNm1]);
                    newD2 = calculDist(newX2,newY2,tabXCP[indNp1],tabYCP[indNp1]);
                    if(newD1 > newD2) {
                        newX[i] = newX1;
                        newY[i] = newY1;
                    } else {
                        newX[i] = newX2;
                        newY[i] = newY2;
                    }   
                    fprintf(stderr, "i : %d, XCP : %d, YCP : %d, distN : %f, distO : %f, angle : %f, newX : %f, newY : %f, \n", i, tabXCP[i], tabYCP[i], distNext[i],distOpp[i], angle[i], newX[i], newY[i]);
                }
                
            } else {
                tabXCP[nbPts] = next_checkpoint_x;
                tabYCP[nbPts] = next_checkpoint_y;
                //fprintf(stderr, "CP numero : %d \n", nbPts);
                //fprintf(stderr, "Valeur x : %d \n", tabXCP[nbPts]);
                //fprintf(stderr, "Valeur y : %d \n", tabYCP[nbPts]);
                nbPts++;
            }
        }
        
        
        if((next_checkpoint_x != last_checkpoint_x) && (next_checkpoint_y != last_checkpoint_y) && !firstLap ){
            // ceux de base
            last_checkpoint_x = next_checkpoint_x;
            last_checkpoint_y = next_checkpoint_y;
            // cherche indice de la cible
            indiceCible ++;
            indiceCible = indiceCible % nbPts;
            if (indiceCible == 1) {
                nbLap ++;
            }
            //fprintf(stderr, "Bip Bip %d \n", indiceCible);
            if (nbLap == 3 && indiceCible == 0) {
                lastCP = true;
                modif = false;
            } else {
                modif = newAcv[indiceCible];
            }
        }
        
        if(modif && speed < 350 && power == 0){
            // après le freinage on plonge à la corde
            modif = false;
        }
        
        // calcul fct cible + distance
        if (modif && modifactif) {
            next_checkpoint_x = newX [indiceCible];
            next_checkpoint_y = newY [indiceCible];
            nextCheckpointDist = calculDist(x,y,newX [indiceCible],newY [indiceCible]) - 700;
        }
        // Write an action using printf(). DON'T FORGET THE TRAILING \n
        // To debug: fprintf(stderr, "Debug messages...\n");
        if ((!newAcv[indiceCible] && !firstLap) || lastCP){
            vlim = 700;
        } else {
            vlim = nextCheckpointDist * 0.15 + 220;
        }
        if (vlim < 250) {
            vlim = 250;
        }
        //speed = sqrt((x-lastx)*(x-lastx) + (y-lasty)*(y-lasty));
        speed = calculDist(x,y,lastx,lasty);
        fprintf(stderr, "%d,%d,%d,%f,%d,%d,%f,%d,%d,%d\n", power, x, y, speed, next_checkpoint_x, next_checkpoint_y, vlim, nextCheckpointDist, nextCheckpointAngle, modif);
        
        if ((speed < vlim && abs(nextCheckpointAngle) < 20) || round < 2) {
            power = 100;
        } else {
            if (speed > vlim){
                power = 0;
            } else {
                power = 60;
            }
        }
        lastx= x;
        lasty = y;
        // Edit this line to output the target position
        // and thrust (0 <= thrust <= 100)
        // i.e.: "x y thrust"
        if (power > 90 && nextCheckpointDist > 7000 && dispoBoost) {
            printf("%d %d BOOST\n", next_checkpoint_x, next_checkpoint_y);
            dispoBoost = false;
        } else {
            printf("%d %d %d\n", next_checkpoint_x, next_checkpoint_y, power);
        }

        round ++;
        
    }

    return 0;
}
